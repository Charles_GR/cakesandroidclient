package com.waracle.androidtest.model.data;

public class Cake
{

    private String title;
    private String description;
    private String imageURL;
    private int insertionIndex;

    public Cake(String title, String description, String imageURL, int insertionIndex)
    {
        this.title = title;
        this.description = description;
        this.imageURL = imageURL;
        this.insertionIndex = insertionIndex;
    }

    public String getTitle()
    {
        return title;
    }

    public String getDescription()
    {
        return description;
    }

    public String getImageURL()
    {
        return imageURL;
    }

    public int getInsertionIndex()
    {
        return insertionIndex;
    }

}
