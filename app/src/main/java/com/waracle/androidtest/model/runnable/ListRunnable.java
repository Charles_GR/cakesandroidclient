package com.waracle.androidtest.model.runnable;

import java.util.List;

public interface ListRunnable<T>
{

    void run(List<T> list);

}