package com.waracle.androidtest.model;

import android.app.Application;
import com.waracle.androidtest.model.data.ImageCache;

public class CakeApplication extends Application
{

    private static CakeApplication instance;
    private ImageCache imageCache;

    public static CakeApplication getInstance()
    {
        return instance;
    }

    public ImageCache getImageCache()
    {
        return imageCache;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        instance = this;
        imageCache = new ImageCache();
    }

}
