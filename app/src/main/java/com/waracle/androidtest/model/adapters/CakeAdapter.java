package com.waracle.androidtest.model.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.*;
import android.widget.*;
import com.waracle.androidtest.model.data.Cake;
import com.waracle.androidtest.processing.tasks.FetchImageTask;
import com.waracle.androidtest.R;
import java.util.ArrayList;
import java.util.List;

public class CakeAdapter extends FilterAdapter<Cake>
{

    public static final String[] sortFields = new String[]{"Insertion", "Title"};
    public static final String[] searchFields = new String[]{"Title", "Description"};

    public CakeAdapter(Context context)
    {
        super(context, R.layout.list_item_cake, new ArrayList<Cake>());
        setComparator(new CakeComparator());
    }

    public CakeAdapter(Context context, List<Cake> cakes)
    {
        super(context, R.layout.list_item_cake, cakes);
        setComparator(new CakeComparator());
    }

    @SuppressWarnings("ConstantConditions")
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent)
    {
        ViewHolder viewHolder;
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_cake, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvTitle = convertView.findViewById(R.id.tvTitle);
            viewHolder.tvDesc = convertView.findViewById(R.id.tvDesc);
            viewHolder.ivImage = convertView.findViewById(R.id.ivImage);
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder)convertView.getTag();
        }
        Cake cake = getItem(position);
        viewHolder.tvTitle.setText(cake.getTitle());
        viewHolder.tvDesc.setText(cake.getDescription());
        new FetchImageTask(cake.getImageURL(), viewHolder.ivImage).execute();
        return convertView;
    }

    @Override
    protected void performSearch(String searchField, String searchQuery)
    {
        for(Cake cake : items)
        {
            switch(searchField)
            {
                case "Title":
                    if(cake.getTitle().contains(searchQuery))
                    {
                        filteredItems.add(cake);
                    }
                    break;
                case "Description":
                    if(cake.getDescription().contains(searchQuery))
                    {
                        filteredItems.add(cake);
                    }
                    break;
            }
        }
    }

    private class CakeComparator extends ItemComparator
    {

        @Override
        public int compare(Cake cakeA, Cake cakeB)
        {
            int compare = 0;
            switch(sortField)
            {
                case "Insertion":
                    compare = Integer.valueOf(cakeA.getInsertionIndex()).compareTo(cakeB.getInsertionIndex());
                    break;
                case "Title":
                    compare = cakeA.getTitle().compareTo(cakeB.getTitle());
                    break;
            }
            return sortOrdering.equals("Ascending") ? compare : -1 * compare;
        }

    }

    private static class ViewHolder
    {

        private TextView tvTitle;
        private TextView tvDesc;
        private ImageView ivImage;

    }

}