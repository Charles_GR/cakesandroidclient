package com.waracle.androidtest.model.data;

import android.graphics.Bitmap;
import java.util.LinkedHashMap;

public class ImageCache
{

    private ImageCacheMap images;

    public ImageCache()
    {
        images = new ImageCacheMap(20);
    }

    public boolean hasImage(String url)
    {
        return images.containsKey(url);
    }

    public Bitmap getImage(String url)
    {
        return images.get(url);
    }

    public void addImage(String url, Bitmap bitmap)
    {
        images.put(url, bitmap);
    }

    public void clearImages()
    {
        images.clear();
    }

    private class ImageCacheMap extends LinkedHashMap<String, Bitmap>
    {

        private int maxCapacity;

        private ImageCacheMap(int maxCapacity)
        {
            super(maxCapacity);
            this.maxCapacity = maxCapacity;
        }

        @Override
        protected boolean removeEldestEntry(Entry<String, Bitmap> eldest)
        {
            return this.size() == maxCapacity;
        }

    }

}
