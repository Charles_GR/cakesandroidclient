package com.waracle.androidtest.view;

import android.app.ListActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.WindowManager;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import com.waracle.androidtest.R;
import com.waracle.androidtest.model.CakeApplication;
import com.waracle.androidtest.model.adapters.CakeAdapter;
import com.waracle.androidtest.model.adapters.FilterAdapter;
import com.waracle.androidtest.model.data.Cake;
import com.waracle.androidtest.model.runnable.ListRunnable;
import com.waracle.androidtest.processing.tasks.FetchCakesTask;
import java.util.List;

public class CakeListActivity extends ListActivity implements OnClickListener, OnLongClickListener, OnItemSelectedListener, TextWatcher
{

    private EditText etSearchQuery;
    private ListView lvCakes;
    private ProgressBar pbProgress;
    private Spinner spnSortField;
    private Spinner spnSortOrdering;
    private Spinner spnSearchField;
    private TextView tvErrorMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cake_list);
        etSearchQuery = findViewById(R.id.etSearchQuery);
        lvCakes = findViewById(android.R.id.list);
        pbProgress = findViewById(R.id.pbProgress);
        spnSortField = findViewById(R.id.spnSortField);
        spnSortOrdering = findViewById(R.id.spnSortOrdering);
        spnSearchField = findViewById(R.id.spnSearchField);
        tvErrorMessage = findViewById(R.id.tvErrorMessage);
        etSearchQuery.addTextChangedListener(this);
        spnSortField.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_item, CakeAdapter.sortFields));
        spnSortField.setOnItemSelectedListener(this);
        spnSortOrdering.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_item, FilterAdapter.sortOrderings));
        spnSortOrdering.setOnItemSelectedListener(this);
        spnSearchField.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_item, CakeAdapter.searchFields));
        spnSearchField.setOnItemSelectedListener(this);
        if(getActionBar() != null && getActionBar().getCustomView() == null)
        {
            View customView = View.inflate(this, R.layout.action_layout_main, null);
            TextView tvTitle = customView.findViewById(R.id.tvTitle);
            ImageView ivRefresh = customView.findViewById(R.id.ivRefresh);
            tvTitle.setText(R.string.app_name);
            ivRefresh.setOnClickListener(this);
            ivRefresh.setOnLongClickListener(this);
            getActionBar().setDisplayShowCustomEnabled(true);
            getActionBar().setCustomView(customView);
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        refreshList();
    }

    @Override
    public void onClick(View view)
    {
        if(view.getId() == R.id.ivRefresh)
        {
            CakeApplication.getInstance().getImageCache().clearImages();
            refreshList();
        }
    }

    @Override
    public boolean onLongClick(View view)
    {
        if(view.getId() == R.id.ivRefresh)
        {
            Toast.makeText(this, R.string.refresh, Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
    {
        searchAndSortCakes();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView)
    {

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {
        searchAndSortCakes();
    }

    @Override
    public void afterTextChanged(Editable editable)
    {

    }

    private void searchAndSortCakes()
    {
        CakeAdapter cakeAdapter = (CakeAdapter)lvCakes.getAdapter();
        if(cakeAdapter != null)
        {
            cakeAdapter.searchItems(spnSearchField.getSelectedItem().toString(), etSearchQuery.getText().toString());
            cakeAdapter.sortItems(spnSortField.getSelectedItem().toString(), spnSortOrdering.getSelectedItem().toString());
        }
    }

    private void refreshList()
    {
        lvCakes.setAdapter(new CakeAdapter(this));
        tvErrorMessage.setVisibility(View.GONE);
        pbProgress.setVisibility(View.VISIBLE);
        new FetchCakesTask(fetchCakesOnSuccess, fetchCakesOnError).execute();
    }

    private final ListRunnable<Cake> fetchCakesOnSuccess = new ListRunnable<Cake>()
    {
        @Override
        public void run(List<Cake> cakes)
        {
            lvCakes.setAdapter(new CakeAdapter(CakeListActivity.this, cakes));
            searchAndSortCakes();
            pbProgress.setVisibility(View.GONE);
        }
    };

    private final Runnable fetchCakesOnError = new Runnable()
    {
        @Override
        public void run()
        {
            pbProgress.setVisibility(View.GONE);
            tvErrorMessage.setVisibility(View.VISIBLE);
        }
    };

}