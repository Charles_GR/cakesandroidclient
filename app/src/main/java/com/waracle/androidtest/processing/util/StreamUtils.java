package com.waracle.androidtest.processing.util;

import java.io.*;
import java.util.ArrayList;

public final class StreamUtils
{

    private StreamUtils()
    {
        throw new UnsupportedOperationException();
    }

    public static byte[] readUnknownFully(InputStream stream) throws IOException
    {
        ArrayList<Byte> data = new ArrayList<>();
        int result = stream.read();
        while(result != -1)
        {
            data.add((byte)result);
            result = stream.read();
        }
        byte[] bytes = new byte[data.size()];
        for(int i = 0; i < bytes.length; i++)
        {
            bytes[i] = data.get(i);
        }
        return bytes;
    }

    public static void close(Closeable closeable)
    {
        if(closeable != null)
        {
            try
            {
                closeable.close();
            }
            catch(IOException e)
            {

            }
        }
    }

}
