package com.waracle.androidtest.processing.util;

import com.waracle.androidtest.model.data.Cake;
import org.json.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public final class CakeFetcher
{

    private static final String JSON_URL = "https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json";

    private CakeFetcher()
    {
        throw new UnsupportedOperationException();
    }

    public static List<Cake> fetchCakes() throws IOException, JSONException
    {
        HttpURLConnection urlConnection = (HttpURLConnection)new URL(JSON_URL).openConnection();
        try
        {
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String charset = parseCharset(urlConnection.getRequestProperty("Content-Type"));
            String jsonText = new String(StreamUtils.readUnknownFully(in), charset);
            JSONArray array = new JSONArray(jsonText);
            List<Cake> cakes = new ArrayList<>();
            for(int i = 0; i < array.length(); i++)
            {
                JSONObject object = array.getJSONObject(i);
                cakes.add(new Cake(object.getString("title"), object.getString("desc"), object.getString("image"), i));
            }
            return cakes;
        }
        finally
        {
            urlConnection.disconnect();
        }
    }

    private static String parseCharset(String contentType)
    {
        if(contentType != null)
        {
            String[] params = contentType.split(",");
            for(int i = 1; i < params.length; i++)
            {
                String[] pair = params[i].trim().split("=");
                if(pair.length == 2 && pair[0].equals("charset"))
                {
                    return pair[1];
                }
            }
        }
        return "UTF-8";
    }

}
