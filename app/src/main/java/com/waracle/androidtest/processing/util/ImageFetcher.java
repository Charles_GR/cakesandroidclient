package com.waracle.androidtest.processing.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidParameterException;

public final class ImageFetcher
{

    private ImageFetcher()
    {
        throw new UnsupportedOperationException();
    }

    public static Bitmap fetchImage(String url) throws IOException
    {
        if(TextUtils.isEmpty(url))
        {
            throw new InvalidParameterException("URL is empty!");
        }
        HttpURLConnection connection = (HttpURLConnection)new URL(url).openConnection();
        InputStream inputStream = null;
        try
        {
            try
            {
                inputStream = connection.getInputStream();
            }
            catch(IOException e)
            {
                inputStream = connection.getErrorStream();
            }
            byte[] data = StreamUtils.readUnknownFully(inputStream);
            return BitmapFactory.decodeByteArray(data, 0, data.length);
        }
        finally
        {
            StreamUtils.close(inputStream);
            connection.disconnect();
        }
    }

}
