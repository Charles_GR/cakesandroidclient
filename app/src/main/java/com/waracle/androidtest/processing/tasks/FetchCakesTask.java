package com.waracle.androidtest.processing.tasks;

import android.os.AsyncTask;
import com.waracle.androidtest.model.data.Cake;
import com.waracle.androidtest.model.runnable.ListRunnable;
import com.waracle.androidtest.processing.util.CakeFetcher;
import org.json.JSONException;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FetchCakesTask extends AsyncTask<Void, Void, List<Cake>>
{

    private boolean success;
    private ListRunnable<Cake> onSuccess;
    private Runnable onError;

    public FetchCakesTask(ListRunnable<Cake> onSuccess, Runnable onError)
    {
        success = true;
        this.onSuccess = onSuccess;
        this.onError = onError;
    }

    @Override
    protected List<Cake> doInBackground(Void... voids)
    {
        List<Cake> cakes = new ArrayList<>();
        try
        {
            cakes = CakeFetcher.fetchCakes();
        }
        catch(IOException | JSONException e)
        {
            success = false;
        }
        return cakes;
    }

    @Override
    protected void onPostExecute(List<Cake> cakes)
    {
        if(success)
        {
            onSuccess.run(cakes);
        }
        else
        {
            onError.run();
        }
    }

}
