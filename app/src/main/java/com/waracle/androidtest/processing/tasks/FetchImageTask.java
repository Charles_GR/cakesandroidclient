package com.waracle.androidtest.processing.tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;
import com.waracle.androidtest.R;
import com.waracle.androidtest.model.CakeApplication;
import com.waracle.androidtest.processing.util.ImageFetcher;
import java.io.IOException;

public class FetchImageTask extends AsyncTask<Void, Void, Bitmap>
{

    private static final Bitmap loading = BitmapFactory.decodeResource(CakeApplication.getInstance().getResources(), R.mipmap.loading);
    private String imageURL;
    private ImageView imageView;

    public FetchImageTask(String imageURL, ImageView imageView)
    {
        this.imageURL = imageURL;
        this.imageView = imageView;
    }

    @Override
    protected void onPreExecute()
    {
        imageView.setImageBitmap(loading);
    }

    @Override
    protected Bitmap doInBackground(Void... voids)
    {
        try
        {
            if(CakeApplication.getInstance().getImageCache().hasImage(imageURL))
            {
                return CakeApplication.getInstance().getImageCache().getImage(imageURL);
            }
            else
            {
                Bitmap bitmap = ImageFetcher.fetchImage(imageURL);
                CakeApplication.getInstance().getImageCache().addImage(imageURL, bitmap);
                return bitmap;
            }
        }
        catch(IOException e)
        {
            return null;
        }
    }

    @Override
    protected void onPostExecute(Bitmap bitmap)
    {
        imageView.setImageBitmap(bitmap);
    }

}
